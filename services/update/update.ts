import { parseContent } from '../..';
import { Type } from '../../interfaces/types';

type IProperties = {
    properties: {
        [property: string]: {
            [key in Type]: {
                [type: string]: string | number | Date | boolean
            }
        }
    }
}

async function update(notionClient: any, page: string, properties: IProperties) {
    
}