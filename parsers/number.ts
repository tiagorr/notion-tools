import { INotionNumber, INumber } from '../interfaces/number';

export default function parseNumber(content: INotionNumber): INumber {
    return content.number;
}
