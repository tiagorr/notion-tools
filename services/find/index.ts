import { parseContent } from '../..';
import { IFilter } from './filter';

async function find(notionClient: any, database: string, filter: IFilter = { and: [], or: [] }) {
    const results = [];

    const payload = {
        path: `databases/${database}/query`,
        method: 'POST',
        body: {filter: filter || { and: [], or: [] }},
    };

    try {
        const response = await notionClient.request(payload);
        for (const result of response.results) {
            const notionProperties = result.properties;
            const resultProperties: any = {
                pageId: result.id
            };
            for (const property of Object.keys(notionProperties)) {
                resultProperties[property] = await parseContent(
                    notionProperties[property],
                    notionProperties[property].type,
                );
            }

            results.push(resultProperties);
        }

        return results;
    } catch (error) {
        throw new Error('✨ ~ notion-tools ~ file: find.ts ~ line 32 ~ find ~ error' + error);
    }
}

async function findById(notionClient: any, pageId: string) {
    const payload = {
        path: `pages/${pageId}`,
        method: 'GET'
    }

    try {
        const response = await notionClient.request(payload);
        const resultProperties: any = { pageId };
        
        for (const property of Object.keys(response.properties)) {
            resultProperties[property] = await parseContent(
                response.properties[property],
                response.properties[property].type,
            );
        }

        return resultProperties;
    } catch(error) {
        throw new Error('✨ ~ notion-tools ~ file: find.ts ~ line 55 ~ findByPageId ~ error' + error);
    }
}

async function findOne(notionClient: any, database: string, filter: IFilter) {
    try {
        const results = await find(notionClient, database, filter);
        return results[0] || undefined;
    } catch (error) {
        throw new Error('✨ ~ notion-tools ~ file: find.ts ~ line 64 ~ findOne ~ error' + error);
    }
}

export default { find, findOne, findById };
