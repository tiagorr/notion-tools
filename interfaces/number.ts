export interface INotionNumber {
    id: string;
    type: 'number';
    number: number;
}

export type INumber = number;

export interface INumberFilter {
    number: {
        equals?: number;
        does_not_equal?: number;
        greater_than?: number;
        less_than?: number;
        greater_than_or_equal_to?: number;
        less_than_or_equal_to?: number;
        is_empty?: true;
        is_not_empty?: true;
    };
}
