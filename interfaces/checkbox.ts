export interface INotionCheckbox {
    id: string;
    type: 'checkbox';
    checkbox: boolean;
}

export type ICheckbox = boolean;

export interface ICheckboxFilter {
    checkbox: {
        equals?: boolean;
        does_not_equal?: boolean;
    };
}
