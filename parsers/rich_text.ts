import { INotionRichText, IRichText } from '../interfaces/rich_text';
import { IText } from '../interfaces/text';

export default function parseRichText(content: INotionRichText): IRichText {
    const richTexts: IText[] = content.rich_text.map((item) => ({
        text: item.text.content,
        href: item.href,
        plain_text: item.plain_text,
        properties: item.annotations,
    }));

    return { content: richTexts, plain_text: richTexts.map((item) => item.plain_text).join('\n') };
}
