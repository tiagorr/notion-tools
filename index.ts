import { Type } from './interfaces/types';
import find from './services/find';

export async function parseContent(content: any, type: Type) {
    return (await import(`./parsers/${type}`)).default(content);
}

export { find };
