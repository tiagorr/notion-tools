import { INotionMultiSelect, IMultiSelect } from '../interfaces/multi_select';

export default function parseMultiSelect(content: INotionMultiSelect): IMultiSelect {
    return content.multi_select;
}
