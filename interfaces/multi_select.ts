import { INotionSelect, ISelect } from './select';

export interface INotionMultiSelect {
    id: string;
    type: 'multi_select';
    multi_select: {
        id: string;
        name: string;
        color: string;
    }[];
}

export type IMultiSelect = ISelect[];

export interface IMultiSelectFilter {
    property: string;
    multi_select: {
        contains?: string;
        does_not_contain?: string;
        is_empty?: true;
        is_not_empty?: true;
    };
}
