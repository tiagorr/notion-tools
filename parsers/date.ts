import { INotionDate, IDate } from '../interfaces/date';

export default function parseDate(content: INotionDate): IDate {
    const dates: IDate = {
        start: new Date(content.date.start),
        end: content.date.end ? new Date(content.date.end) : null,
    };

    return dates;
}
