export interface INotionDate {
    id: string;
    type: 'date';
    date: {
        start: string;
        end: string | null;
    };
}

export interface IDate {
    start: Date;
    end: Date | null;
}

export type IDateFilter = {
    date: {
        equals?: string;
        does_not_equal?: string;
        before?: string;
        after?: string;
        on_or_before?: string;
        is_empty?: true;
        is_not_empty?: true;
        on_or_after?: string;
        past_week?: {};
        past_month?: {};
        past_year?: {};
        next_week?: {};
        next_month?: {};
        next_year?: {};
    };
};
