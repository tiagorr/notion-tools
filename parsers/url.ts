import { INotionUrl, IUrl } from '../interfaces/url';

export default function parseUrl(content: INotionUrl): IUrl {
    return content.url;
}
