import { INotionText, IText } from './text';

export interface INotionTitle {
    id: string;
    type: 'title';
    title: INotionText[];
}

export interface ITitle {
    plain_text: string;
    content: IText[];
}
