import { INotionText, IText } from './text';

export interface INotionRichText {
    id: string;
    type: 'rich_text';
    rich_text: INotionText[];
}

export interface IRichText {
    plain_text: string;
    content: IText[];
}
