export interface INotionText {
    type: 'text';
    text: {
        content: string;
        link: {
            url: string;
        } | null;
    };
    annotations: {
        bold: boolean;
        italic: boolean;
        strikethrough: boolean;
        underline: boolean;
        code: boolean;
        color: string;
    };
    plain_text: string;
    href: string | null;
}

export interface IText {
    text: string;
    href: string | null;
    plain_text: string;
    properties: {
        bold: boolean;
        italic: boolean;
        strikethrough: boolean;
        underline: boolean;
        code: boolean;
        color: string;
    };
}

export type ITextFilter = {
    text: {
        equals?: string;
        does_not_equal?: string;
        contains?: string;
        does_not_contain?: string;
        starts_with?: string;
        ends_with?: string;
        is_empty?: true;
        is_not_empty?: true;
    };
};
