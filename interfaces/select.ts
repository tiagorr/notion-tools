export interface INotionSelect {
    id: string;
    type: 'select';
    select: {
        id: string;
        name: string;
        color: string;
    };
}

export interface ISelect {
    id: string;
    name: string;
    color: string;
}

export interface ISelectFilter {
    property: string;
    select: {
        equals?: string;
        does_not_equal?: string;
        is_empty?: true;
        is_not_empty?: true;
    };
}
