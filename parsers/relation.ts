import { INotionRelation, IRelation } from '../interfaces/relation';

export default function parseRelation(content: INotionRelation): IRelation {
    return content.relation.map((relation) => relation.id);
}
