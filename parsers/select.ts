import { INotionSelect, ISelect } from '../interfaces/select';

export default function parseSelect(content: INotionSelect): ISelect {
    return content.select;
}
