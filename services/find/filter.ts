import { ICheckboxFilter } from '../../interfaces/checkbox';
import { IDateFilter } from '../../interfaces/date';
import { IMultiSelectFilter } from '../../interfaces/multi_select';
import { ISelectFilter } from '../../interfaces/select';
import { INumberFilter } from '../../interfaces/number';
import { IRelationFilter } from '../../interfaces/relation';
import { ITextFilter } from '../../interfaces/text';

export type IFilterItem =
    | ICheckboxFilter
    | IDateFilter
    | IMultiSelectFilter
    | ISelectFilter
    | INumberFilter
    | IRelationFilter
    | ITextFilter;

export interface IFilter {
    and: ({ property: string } & IFilterItem)[];
    or: ({ property: string } & IFilterItem)[];
}
