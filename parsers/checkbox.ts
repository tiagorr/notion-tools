import { INotionCheckbox, ICheckbox } from '../interfaces/checkbox';

export default function parseCheckbox(content: INotionCheckbox): ICheckbox {
    return content.checkbox;
}
