export interface INotionRelation {
    id: string;
    type: 'relation';
    relation: {
        id: string;
    }[];
}

export type IRelation = string[];

export interface IRelationFilter {
    property: string;
    relation: {
        contains?: string;
        does_not_contain?: string;
        is_empty?: true;
        is_not_empty?: true;
    };
}
