export type Type = 'checkbox' | 'date' | 'multi_select' | 'number' | 'rich_text' | 'title' | 'url' | 'relation';
