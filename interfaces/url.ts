export interface INotionUrl {
    id: string;
    type: 'url';
    url: string;
}

export type IUrl = string;
