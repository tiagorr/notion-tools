import { INotionTitle, ITitle } from '../interfaces/title';
import { IText } from '../interfaces/text';

export default function parseTitle(content: INotionTitle): ITitle {
    const richTexts: IText[] = content.title.map((item) => ({
        text: item.text.content,
        href: item.href,
        plain_text: item.plain_text,
        properties: item.annotations,
    }));

    return { content: richTexts, plain_text: richTexts.map((item) => item.plain_text).join('\n') };
}
